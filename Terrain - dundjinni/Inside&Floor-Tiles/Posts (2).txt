Topic 'Couple of Quick Floor Tiles' last changed on 13.12.2011 06:52:00 started by User Eanwulf (http://www.dundjinni.com/forums/forum_posts.asp?TID=13323&PN=3&get=last) 
With 17 posts.
****************************************************************************************************
|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 03.12.2011 20:24:00                             |
|____________________________________________________________________________________________________|
|Made a series of templates in Genetica to use as random "generic" floor tiles and had fun slapping  |
|together a couple of quick textures to fill them with.                                              |
|                                                                                                    |
|If you like the patterns themselves (though generic) I can simply add additional textures to them   |
|in this thread as time and necessity allows.                                                        |
|                                                                                                    |
|Oh Yeah! There are 20 tiles in the complete set. Four variants of a basic 4 tile set with 2         |
|additional tiles (and a variant of each to help break up the pattern).                              |
|                                                                                                    |
|With them saved as a .gtx for Genetica, I can simply creature a new texture and modify the Cut n    |
|Tile lab I have applied to it, adjusting mortar color,                                              |
|etc.                                                                                                |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                           Has no files.                                            |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 03.12.2011 20:30:00                             |
|____________________________________________________________________________________________________|
|First Tile is a Moon Rock Texture I liked.                                                          |
|                                                                                                    |
|Files                                                                                               |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/EB7_moon_rock_ean_pt1.zip]EB7_moon_rock_ean_pt1|
|.zip[/URL]                                                                                          |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/39A_moon_rock_ean_pt2.zip]39A_moon_rock_ean_pt2|
|.zip[/URL]                                                                                          |
|                                                                                                    |
|Close Up Shot of Texture                                                                            |
|[IMG=Green-rock2.jpg]                                                                               |
|                                                                                                    |
|Snapshot of It Tiled                                                                                |
|[IMG=Green-rock.jpg]                                                                                |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 2 images and 2 archives.                                    |
| * Green-rock2.jpg                                                                                  |
| * Green-rock.jpg                                                                                   |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 03.12.2011 20:38:00                             |
|____________________________________________________________________________________________________|
|The next is a Toad Stone texture.                                                                   |
|                                                                                                    |
|Files                                                                                               |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/A21_toad_stone_ean_pt1.zip]A21_toad_stone_ean_p|
|t1.zip[/URL]                                                                                        |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/374_toad_stone_ean_pt2.zip]374_toad_stone_ean_p|
|t2.zip[/URL]                                                                                        |
|                                                                                                    |
|Close Up Shot of Texture                                                                            |
|[IMG=Toad-stone2.jpg]                                                                               |
|                                                                                                    |
|Snapshot of It Tiled                                                                                |
|[IMG=Toad-stone.jpg]                                                                                |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 2 images and 2 archives.                                    |
| * Toad-stone2.jpg                                                                                  |
| * Toad-stone.jpg                                                                                   |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                             Post by User Bogie on 03.12.2011 21:11:00                              |
|____________________________________________________________________________________________________|
|These are fantastic![=D>][=D>][=D>]                                                                 |
|Great                                                                                               |
|Textures![:D]                                                                                       |
|__________________                                                                                  |
|Meddle not in the affairs of Dragons, for you are crunchy and taste good with                       |
|ketchup.                                                                                            |
|____________________________________________________________________________________________________|
|                                           Has no files.                                            |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 03.12.2011 21:26:00                             |
|____________________________________________________________________________________________________|
|Thanks! I've been having fun with the Canvas option of Genetica and have been making all kinds of   |
|shapes and patterns. Once I finally learn the damned thing, I might be dangerous lol. Who knows ;)  |
|                                                                                                    |
|Using the Synthesis lab now to create a neutral colored stone texture. Will upload it as soon as    |
|I'm done.                                                                                           |
|                                                                                                    |
|The program's quite plug n play once you get it                                                     |
|right.                                                                                              |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                           Has no files.                                            |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 03.12.2011 12:01:00                             |
|____________________________________________________________________________________________________|
|Sorry. This tile took me twice as long to complete because I was trying to save the textures to a   |
|secured hard drive and as such, they were put into limbo lol.                                       |
|                                                                                                    |
|Anyways, I used the Synthesis node to sample a texture and generate one from it.                    |
|                                                                                                    |
|I give you, Neutral Stone.                                                                          |
|                                                                                                    |
|Files                                                                                               |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/C66_neutral_stone_ean_pt1.zip]C66_neutral_stone|
|_ean_pt1.zip[/URL]                                                                                  |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/DZ9_neutral_stone_ean_pt2.zip]DZ9_neutral_stone|
|_ean_pt2.zip[/URL]                                                                                  |
|                                                                                                    |
|Close Up Shot of Texture                                                                            |
|[IMG=Neutral-stone2.jpg]                                                                            |
|                                                                                                    |
|Snapshot of It Tiled                                                                                |
|[IMG=Neutral-stone.jpg]                                                                             |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 2 images and 2 archives.                                    |
| * Neutral-stone2.jpg                                                                               |
| * Neutral-stone.jpg                                                                                |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 03.12.2011 12:28:00                             |
|____________________________________________________________________________________________________|
|Heh. Couldn't resist. Made a quick map with this last tile set. Really wish this Forum had Spoiler  |
|mode. Would help to save a lot of space when posting stuff :)                                       |
|                                                                                                    |
|[IMG=Goofy.jpg]                                                                                     |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 1 images and 0 archives.                                    |
| * Goofy.jpg                                                                                        |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Ravenbow on 05.12.2011 06:52:00                            |
|____________________________________________________________________________________________________|
|These are great. Thank you.                                                                         |
|                                                                                                    |
|Could I request a reddish version of the neutral one?                                               |
|                                                                                                    |
|Not quite brimstone but close. Similar in feel to[IMG=BrickRound70_CG_bg-a.png]but just a tad       |
|lighter?                                                                                            |
|____________________________________________________________________________________________________|
|                                   With 1 images and 0 archives.                                    |
| * BrickRound70_CG_bg-a.png                                                                         |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 05.12.2011 07:35:00                             |
|____________________________________________________________________________________________________|
|You talking about something like this?                                                              |
|                                                                                                    |
|[IMG=Texture_1.png]                                                                                 |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 1 images and 0 archives.                                    |
| * Texture_1.png                                                                                    |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 05.12.2011 08:28:00                             |
|____________________________________________________________________________________________________|
|If so, then.                                                                                        |
|                                                                                                    |
|Files                                                                                               |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/CFF_ravens_brimstone_ean_pt1.zip]CFF_ravens_bri|
|mstone_ean_pt1.zip[/URL]                                                                            |
|[URL=http://www.dundjinni.com/forums/uploads/Eanwulf/FCA_ravens_brimstone_ean_pt2.zip]FCA_ravens_bri|
|mstone_ean_pt2.zip[/URL]                                                                            |
|                                                                                                    |
|Close Up Shot of Tiles                                                                              |
|[IMG=Raven2.jpg]                                                                                    |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 1 images and 2 archives.                                    |
| * Raven2.jpg                                                                                       |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Ravenbow on 05.12.2011 14:33:00                            |
|____________________________________________________________________________________________________|
|Perfect. Thank                                                                                      |
|you!!                                                                                               |
|____________________________________________________________________________________________________|
|                                           Has no files.                                            |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 12.12.2011 18:52:00                             |
|____________________________________________________________________________________________________|
|Does anyone like this?                                                                              |
|                                                                                                    |
|I've found a couple of funky floor tile patterns I like and have been mucking around with them in   |
|Photoshop. If I can finish learning how to use this blasted Canvas feature in Genetica I can simply |
|slap the template programming to random textures. Meanwhile it's a tad bit more painstaking to      |
|complete lol.                                                                                       |
|                                                                                                    |
|[IMG=Funky_marble.png]                                                                              |
|                                                                                                    |
|Example of tile in use                                                                              |
|[IMG=Funky-marble.jpg]                                                                              |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 2 images and 0 archives.                                    |
| * Funky_marble.png                                                                                 |
| * Funky-marble.jpg                                                                                 |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 12.12.2011 20:38:00                             |
|____________________________________________________________________________________________________|
|Another variant in design. Once I get the overall Bevel and shadowing effects, I can create a       |
|series of varied textured tiles if interest is great enough to warrant such.                        |
|                                                                                                    |
|[IMG=Another_variant.png]                                                                           |
|                                                                                                    |
|Tiles in Use                                                                                        |
|[IMG=Another-variant.jpg]                                                                           |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 2 images and 0 archives.                                    |
| * Another_variant.png                                                                              |
| * Another-variant.jpg                                                                              |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                             Post by User Bogie on 12.12.2011 13:26:00                              |
|____________________________________________________________________________________________________|
|Very cool, wicked                                                                                   |
|good.                                                                                               |
|__________________                                                                                  |
|Meddle not in the affairs of Dragons, for you are crunchy and taste good with                       |
|ketchup.                                                                                            |
|____________________________________________________________________________________________________|
|                                           Has no files.                                            |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 12.12.2011 13:49:00                             |
|____________________________________________________________________________________________________|
|Here's the templates I'm using in Photoshop. I just need to figure out how to remove the edged      |
|bevel effects on the edges to give them a nice clean seamless appearance. Only spent like 2 minutes |
|on each tile mucking about but really like the patterns.                                            |
|                                                                                                    |
|The first could be used as a marble hall tile leading into a room containing the second. I dunno, I |
|just really liked the patterns lol.                                                                 |
|                                                                                                    |
|[IMG=New_Texture_1.png]                                                                             |
|[IMG=Texture_2.png]                                                                                 |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 2 images and 0 archives.                                    |
| * Texture_1.png                                                                                    |
| * Texture_2.png                                                                                    |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User Eanwulf on 13.12.2011 02:14:00                             |
|____________________________________________________________________________________________________|
|A quick revamp using Pillowed Embossing this time.                                                  |
|                                                                                                    |
|[IMG=Revamp.png]                                                                                    |
|                                                                                                    |
|Sample of Tile in use                                                                               |
|[IMG=Revamp.jpg]                                                                                    |
|                                                                                                    |
|Not the greatest in the world, but with a gridline placed on top, should                            |
|suffice.                                                                                            |
|__________________                                                                                  |
|"What doesn't kill you, simply makes you walk                                                       |
|funny..."                                                                                           |
|____________________________________________________________________________________________________|
|                                   With 2 images and 0 archives.                                    |
| * Revamp.png                                                                                       |
| * Revamp.jpg                                                                                       |
|____________________________________________________________________________________________________|


|____________________________________________________________________________________________________|
|                            Post by User ProBono on 13.12.2011 06:52:00                             |
|____________________________________________________________________________________________________|
|Both sets are very dramatic and eye catching![smiley20.gif]                                         |
|                                                                                                    |
|The first set may be too "busy" for a big room(?), as                                               |
|it has my eyes flashing about. To the point I thought                                               |
|it might make good direction symbols "this way to the                                               |
|orange room"...                                                                                     |
|[IMG=Funky_marble2_Ew_PB.png][IMG=Funky_marble_Ew_PB.png]                                           |
|...which can be a good thing, no criticism.                                                         |
|                                                                                                    |
|The 2nd set looks fine even without an extra gridline on top.                                       |
|Thanks also for describing the thought process behind making                                        |
|them.                                                                                               |
|____________________________________________________________________________________________________|
|                                   With 2 images and 0 archives.                                    |
| * Funky_marble2_Ew_PB.png                                                                          |
| * Funky_marble_Ew_PB.png                                                                           |
|____________________________________________________________________________________________________|


